import './App.css';
import SideBar from './Components/SideBar';
import DashboardLayout from './Pages/DashboardLayout';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'


function App() {
  return (
    <div >
      {/* <Router>
      <Routes>
        <Route path="/dashboardlayout" exact element={<DashboardLayout />} />
      </Routes>
      </Router> */}
      <DashboardLayout />
    </div>
  );
}

export default App;

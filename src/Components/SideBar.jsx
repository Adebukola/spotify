import React, {useState} from "react";
import { GrHomeRounded } from "react-icons/gr";
import { FcSearch } from "react-icons/fc";
import { SiLibrariesdotio } from "react-icons/si";
import { AiOutlinePlus } from "react-icons/ai";
import { AiOutlineArrowRight } from "react-icons/ai";
import { FaSearch } from "react-icons/fa";
import { MdOutlineArrowDropDown } from "react-icons/md";
import { BsMusicNoteBeamed } from "react-icons/bs";



const SideBar = () => {


  const [showSearch, setshowSearch] = useState(false)
  const toggle = ()=>{
    setshowSearch(!showSearch)
  }


  return (
    <div className=" bg-black w-96  text-white">
      <div className="menu  flex flex-col bg-gray-900 text-white shadow-2xl drop-shadow-md rounded-2xl gap-6 p-10 mb-2">
        <div className="menu-item flex justify-start items-center gap-6  ">
          <GrHomeRounded className="text-white" />
          <span>Home</span>
        </div>
        <div className="menu-item flex justify-start items-center gap-6">
          <FcSearch />
          
          <span>Search</span>

        </div>
      </div>
      <div className="flex flex-col bg-gray-900 text-white shadow-2xl drop-shadow-md rounded-2xl gap-6 p-10">
        <div className="menu-item menu-item flex justify-between items-center ">
        <div className="flex justify-start items-center gap-6">
          <SiLibrariesdotio />
          <span>Your Libary</span>
          </div>
          <div className="flex justify-start gap-6">
          <span>
            <AiOutlinePlus />
          </span>
          <span>
            <AiOutlineArrowRight />
          </span>
          </div>
        </div>
        <div className="menu-item menu-item flex justify-start align-center gap-6">
          <span>Playlist</span>
          <span>Artist</span>
        </div>
        <div className="search-container menu-item flex justify-between items-center gap-6">
          <div className="items-center">
          <FaSearch className="search-icon absolute mt-2 ml-1 " onClick={toggle} />
          {showSearch && (
          <input type="text" placeholder="Search In Your Libary" className=" bg-gray-800 p-1 rounded-xl pl-8"/>
          )}
          </div>
          <div className="menu-item flex justify-start items-center gap-1">
          <span>Recent</span>
          <span>
            <MdOutlineArrowDropDown />
          </span>
          </div>
        </div>
        <div className="menu-item menu-item flex justify-start items-center gap-6">
          <span className='p-4 bg-gray-700'>
            <BsMusicNoteBeamed className='w-8 h-8' />
          </span>
          <span>
            <div>
              <p>My Playlist 1</p>
              <p>Playlist . Adeniji Adebukola</p>
            </div>
          </span>
        </div>
        <div className="menu-item menu-item flex justify-start items-center gap-6">
            <img src="" className='w-12 h-12 bg-gray-700 rounded-full' />
            <span>Ed Sheeran</span>
        </div>
        <div className="menu-item menu-item flex justify-start items-center gap-6">
            <img src="" className='w-12 h-12 bg-gray-700 rounded-full' />
            <span>Teni</span>
        </div>
        <div className="menu-item menu-item flex justify-start items-center gap-6">
            <img src=""  className='w-12 h-12 bg-gray-700 rounded-full'/>
            <span>Simi</span>
        </div>
        <div className="menu-item menu-item flex justify-start items-center gap-6">
            <img src="" className='w-12 h-12 bg-gray-700 rounded-full' />
            <span>Tems</span>
        </div>
      </div>
    </div>
  );
};

export default SideBar;

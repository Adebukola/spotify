import React from "react";
import { BsInstagram } from "react-icons/bs";
import { BsTwitter } from "react-icons/bs";
import { FiFacebook } from "react-icons/fi";

const Footer = () => {
  return (
    <div className="absolute  bottom-0 w-3/4  bg-gray-900 ml-4  shadow-2xl drop-shadow-md rounded-2xl">
      <div className="footer-top  flex justify-between p-8">
        <div className="between-footer flex justify-between w-full mt-4 mb-4">
          <div className="1stslide flex justify-between w-1/2">
            <div className="footer-menu flex flex-col">
              <h4>Company</h4>
              <a className="text-gray-500 font-semibold">About</a>
              <a className="text-gray-500 font-semibold">Jobs</a>
              <a className="text-gray-500 font-semibold">For The Record</a>
            </div>
            <div className="footer-menu  flex flex-col">
              <h4>Communities</h4>
              <a className="text-gray-500 font-semibold">For Artist</a>
              <a className="text-gray-500 font-semibold">Developers</a>
              <a className="text-gray-500 font-semibold">Advertising</a>
              <a className="text-gray-500 font-semibold">Investors</a>
              <a className="text-gray-500 font-semibold">Vendors</a>
              <a className="text-gray-500 font-semibold">Spotify for work</a>
            </div>
            <div className="footer-menu  flex flex-col">
              <h4>Useful as</h4>
              <a className="text-gray-500 font-semibold">Supports</a>
              <a className="text-gray-500 font-semibold">Free Mobile App</a>
            </div>
          </div>
          <div className="2ndslide flex flex-end justify-between gap-4">
            <BsInstagram />
            <BsTwitter />
            <FiFacebook />
          </div>
        </div>
      </div>
      <hr className="bg-gray-300" />
      <div className="flex justify-between mt-6 mb-10">
        <div className="flex gap-2 ">
          <a href="" className="text-gray-500">Legal</a>
          <a href="" className="text-gray-500"> Privacy Center</a>
          <a href="" className="text-gray-500">Privacy Policy</a>
          <a href="" className="text-gray-500">Cookies</a>
          <a href="" className="text-gray-500">About Ads</a>
          <a href="" className="text-gray-500">Accesibility</a>
        </div>
        <div className="text-gray-500">
          <p>© 2023 Spotify AB</p>
        </div>
      </div>
    </div>
  );
};

export default Footer;

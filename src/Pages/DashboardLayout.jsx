import React from 'react'
import SideBar from '../Components/SideBar'
import MusicList from './MusicList'
import Footer from '../Components/Footer'
// import { BrowserRouter, Routes, Route } from "react-router-dom";


const DashboardLayout = () => {
  return (
    <div className="relative flex bg-black p-2 h-full w-full">
    {/* // <div className="flex flex-row bg-black" > */}
        {/* <div className="sidebar absolute"> */}
        <div className='w-1/4'>
            <SideBar />
            </div>
            <div className='w-3/4 h-full'>
            <MusicList />
            </div>
        {/* </div>
        <div className="layout relative">
            <Routes>
                <Route path="/musiclist" element={<MusicList />} />
            </Routes>
        </div> */}
    </div>
  )
}

export default DashboardLayout
import React from "react";
import { IoIosArrowBack } from "react-icons/io";
import { IoIosArrowForward } from "react-icons/io";
import { MdArrowCircleDown } from "react-icons/md";
import { TfiUser } from "react-icons/tfi";
import Footer from "../Components/Footer";

const MusicList = () => {
  return (
    <div className="text-white  bg-gray-900 ml-4 h-full shadow-2xl drop-shadow-md rounded-2xl">
      <div className="music-header p-8 bg-gray-900 shadow-2xl drop-shadow-md rounded-t-2xl fixed w-3/4 flex justify-between self-center z-50">
        <div className="flex gap-4 ">
          <IoIosArrowBack className="rounded-full bg-black h-8 w-8" />
          <IoIosArrowForward className="rounded-full bg-black h-8 w-8" />
        </div>
        <div className="flex gap-6">
          <button className="bg-white text-black text-base font-semibold px-4 py-2 rounded-3xl">
            Upgrade
          </button>
          <button className="flex gap-1 justify-center text-center items-center bg-black text-base font-semibold px-4 py-2 rounded-3xl">
            <MdArrowCircleDown />
            <span>InstallApp</span>{" "}
          </button>
          <button>
            <TfiUser />
          </button>
        </div>
      </div>
      <div className="py-32 px-10">
        <h1 className="text-3xl">Browse all</h1>
        <div className='grid grid-cols-5 gap-6'>
        <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-purple-500'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-orange-500'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-pink-500'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-blue-500'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-gray-500'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-purple-300'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-blue-900'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-pink-200'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-green-900'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-orange-900'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-purple-500'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-purple-500'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-purple-500'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-purple-500'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-purple-500'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-purple-500'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-purple-500'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-purple-500'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-purple-500'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-purple-500'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-purple-500'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-purple-500'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-purple-500'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-purple-500'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-purple-500'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-purple-500'/>
          </a>
          <a href="" className='list-none' >
            <li className='absolute text-2xl mt-4 ml-4'>Podcast</li>
            <img src=""  className='w-40 h-44 rounded-xl bg-purple-500'/>
          </a>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default MusicList;
